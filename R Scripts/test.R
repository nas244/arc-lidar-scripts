dropout_error<- read.csv("H:/Desktop/MAVS/Data/Data2/droput_error_summary_250.csv")
odometry_error<- read.csv("H:/Desktop/MAVS/Data/Data2/odometry_250_error.csv")
lidar_error<- read.csv("H:/Desktop/MAVS/Data/Data2/lidar_50samples_error_summary.csv")
rain_error<- read.csv("H:/Desktop/MAVS/Data/Data2/rain_50samples_error_summary.csv")
dust_error<- read.csv("H:/Desktop/MAVS/Data/Data2/dust_50samples_error_summary.csv")

dropout_mat<-cor(dropout_error, use="pairwise.complete.obs")
odometry_mat<-cor(odometry_error, use="pairwise.complete.obs")
lidar_mat<-cor(lidar_error, use="pairwise.complete.obs")
rain_mat<-cor(rain_error, use="pairwise.complete.obs")
dust_mat<-cor(dust_error, use="pairwise.complete.obs")

total_error <- read.csv("H:/Desktop/MAVS/Data/Data2/rain_dust_lidar_50samples_error_summary.csv")
total_mat<-cor(total_error, use="pairwise.complete.obs")

dust_pointcloud_rate<-data.frame(dust_error[2],dust_error[11])
plot(dust_pointcloud_rate)
fit1<-lm(Point.Cloud.Error~Dust.Rate,dust_pointcloud_rate)
fit2<-lm(Point.Cloud.Error~poly(Dust.Rate,2,raw=TRUE),dust_pointcloud_rate)
fit3<-lm(Point.Cloud.Error~poly(Dust.Rate,3,raw=TRUE),dust_pointcloud_rate)
fit4<-lm(Point.Cloud.Error~poly(Dust.Rate,4,raw=TRUE),dust_pointcloud_rate)
fit5<-lm(Point.Cloud.Error~poly(Dust.Rate,5,raw=TRUE),dust_pointcloud_rate)

x<-seq(0,8,length=40)
lines(x,predict(fit1, data.frame(Dust.Rate=x)), col="red")
lines(x,predict(fit2, data.frame(Dust.Rate=x)), col="blue")
lines(x,predict(fit3, data.frame(Dust.Rate=x)), col="green")
lines(x,predict(fit4, data.frame(Dust.Rate=x)), col="orange")

summary(fit1)
summary(fit2)
summary(fit3)
summary(fit4)
summary(fit5)

anova(fit1,fit2)
anova(fit2,fit3)
anova(fit3,fit4)
anova(fit4,fit5)

dust_pointcloud_rate_for<-fit3
plot(dust_pointcloud_rate_for)

dust_griderror_rate<-data.frame(dust_error[2],dust_error[14])
plot(dust_griderror_rate)

fit1<-lm(Grid.Error~Dust.Rate,dust_griderror_rate)
fit2<-lm(Grid.Error~poly(Dust.Rate,2,raw=TRUE),dust_griderror_rate)
fit3<-lm(Grid.Error~poly(Dust.Rate,3,raw=TRUE),dust_griderror_rate)
fit4<-lm(Grid.Error~poly(Dust.Rate,4,raw=TRUE),dust_griderror_rate)
fit5<-lm(Grid.Error~poly(Dust.Rate,5,raw=TRUE),dust_griderror_rate)

x<-seq(0,.02,length=40)
lines(x,predict(fit1, data.frame(Dust.Rate=x)), col="red")
lines(x,predict(fit2, data.frame(Dust.Rate=x)), col="blue")
lines(x,predict(fit3, data.frame(Dust.Rate=x)), col="green")
lines(x,predict(fit4, data.frame(Dust.Rate=x)), col="orange")
lines(x,predict(fit5, data.frame(Dust.Rate=x)), col="magenta")

anova(fit1,fit2)
anova(fit2,fit3)
anova(fit3,fit4)
anova(fit4,fit5)

summary(fit3)

dust_griderror_rate_for<-fit3

dust_griderror_point<-data.frame(dust_error[11],dust_error[14])
plot(dust_griderror_point)

fit1<-lm(Grid.Error~Point.Cloud.Error,dust_griderror_point)
fit2<-lm(Grid.Error~poly(Point.Cloud.Error,2,raw=TRUE),dust_griderror_point)
fit3<-lm(Grid.Error~poly(Point.Cloud.Error,3,raw=TRUE),dust_griderror_point)
fit4<-lm(Grid.Error~poly(Point.Cloud.Error,4,raw=TRUE),dust_griderror_point)
fit5<-lm(Grid.Error~poly(Point.Cloud.Error,5,raw=TRUE),dust_griderror_point)

x<-seq(0,.8,length=40)
lines(x,predict(fit1, data.frame(Point.Cloud.Error=x)), col="red")
lines(x,predict(fit2, data.frame(Point.Cloud.Error=x)), col="blue")
lines(x,predict(fit3, data.frame(Point.Cloud.Error=x)), col="green")
lines(x,predict(fit4, data.frame(Point.Cloud.Error=x)), col="orange")
lines(x,predict(fit5, data.frame(Point.Cloud.Error=x)), col="magenta")

summary(fit1)
summary(fit2)
summary(fit3)
summary(fit4)
summary(fit5)

anova(fit1,fit2)
anova(fit2,fit3)
anova(fit3,fit4)
anova(fit4,fit5)

dust_griderror_point_for<-fit4

dust_griderror_point_rate<-data.frame(dust_error[1],dust_error[11],dust_error[14])
plot(dust_griderror_point_rate)

fit1<-lm(Grid.Error~Point.Cloud.Error+Dust.Rate,dust_griderror_point_rate)
summary(fit1)
dust_griderror_point_rate_for<-fit1
layout(matrix(c(1,2,3,4),2,2))
plot(fit1)

layout(1,1)
dust_collision_grid<-data.frame(dust_error[14],dust_error[8])
plot(dust_collision_grid$Grid.Error,dust_collision_grid$Collision)

dust_collision_grid$Collision.factor<-factor(dust_collision_grid$Collision, labels=c("N","Y"))
fit<-glm(Collision.factor~Grid.Error, dust_collision_grid, family = binomial)
cdplot(Collision.factor~Grid.Error, dust_collision_grid)
summary(fit)
hoslem.test(dust_collision_grid$Collision,fitted(fit))
layout(matrix(c(1,2,3,4),2,2))
plot(fit)
layout(1,1)

rain_Collision_grid_for<-fit

rain_collision_speed<-data.frame(rain_error[10],rain_error[8])
plot(rain_collision_speed)