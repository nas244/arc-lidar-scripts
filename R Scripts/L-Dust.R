#TESTS FOR LIDAR DUST DATA

# IMPORT FILES ----
setwd("C:/users/nas244/Arc Lidar/arc-lidar-scripts")

dropout_error<- read.csv("Data4/sims_6-10/dropout_error_summary.csv")
RTK_error<- read.csv("Data4/sims_6-10/odom_error_summary.csv")
lidar_error<- read.csv("Data4/sims_6-10/lidar_error_summary.csv")
rain_error<- read.csv("Data4/sims_6-10/rain_error_summary.csv")
dust_error<- read.csv("Data4/sims_6-10/dust_error_summary.csv")

dropout_mat<-cor(dropout_error, use="pairwise.complete.obs")
odometry_mat<-cor(RTK_error, use="pairwise.complete.obs")
lidar_mat<-cor(lidar_error, use="pairwise.complete.obs")
rain_mat<-cor(rain_error, use="pairwise.complete.obs")
dust_mat<-cor(dust_error, use="pairwise.complete.obs")

cor(dust_error)

# RATE ~ POINT CLOUD ERROR ----

dust_pointcloud_rate<-data.frame(dust_error[2],dust_error[11])
plot(dust_pointcloud_rate)

#Create regression fits
fit1<-lm(Point.Cloud.Error~Dust.Rate,dust_pointcloud_rate)
fit2<-lm(Point.Cloud.Error~poly(Dust.Rate,2,raw=TRUE),dust_pointcloud_rate)
fit3<-lm(Point.Cloud.Error~poly(Dust.Rate,3,raw=TRUE),dust_pointcloud_rate)
fit4<-lm(Point.Cloud.Error~poly(Dust.Rate,4,raw=TRUE),dust_pointcloud_rate)
fit5<-lm(Point.Cloud.Error~poly(Dust.Rate,5,raw=TRUE),dust_pointcloud_rate)

#Plot fits
x<-seq(0,30,length=40)
lines(x,predict(fit1, data.frame(Dust.Rate=x)), col="red")
lines(x,predict(fit2, data.frame(Dust.Rate=x)), col="blue")
lines(x,predict(fit3, data.frame(Dust.Rate=x)), col="green")
lines(x,predict(fit4, data.frame(Dust.Rate=x)), col="orange")

summary(fit1)
summary(fit2)
summary(fit3)
summary(fit4)
summary(fit5)

#Analysis of variance between fits
anova(fit1,fit2)
anova(fit2,fit3)
anova(fit3,fit4)
anova(fit4,fit5)

#Plot fit 1
dust_pointcloud_rate_fit<-fit1
layout(matrix(c(1,2,3,4),2,2))
plot(dust_pointcloud_rate_fit)
layout(1,1)

# RATE ~ GRID ERROR ----

dust_griderror_rate<-data.frame(dust_error[2],dust_error[14])
plot(dust_griderror_rate)

#Create regression fits
fit1<-lm(Grid.Error~Dust.Rate,dust_griderror_rate)
fit2<-lm(Grid.Error~poly(Dust.Rate,2,raw=TRUE),dust_griderror_rate)
fit3<-lm(Grid.Error~poly(Dust.Rate,3,raw=TRUE),dust_griderror_rate)
fit4<-lm(Grid.Error~poly(Dust.Rate,4,raw=TRUE),dust_griderror_rate)
fit5<-lm(Grid.Error~poly(Dust.Rate,5,raw=TRUE),dust_griderror_rate)

#Plot fits
x<-seq(0,30,length=40)
lines(x,predict(fit1, data.frame(Dust.Rate=x)), col="red")
lines(x,predict(fit2, data.frame(Dust.Rate=x)), col="blue")
lines(x,predict(fit3, data.frame(Dust.Rate=x)), col="green")
lines(x,predict(fit4, data.frame(Dust.Rate=x)), col="orange")
lines(x,predict(fit5, data.frame(Dust.Rate=x)), col="magenta")

#Analysis of variance between fits
anova(fit1,fit2)
anova(fit2,fit3)
anova(fit3,fit4)
anova(fit4,fit5)

#Plot fit 1
dust_griderror_rate_fit<-fit1
layout(matrix(c(1,2,3,4),2,2))
plot(dust_griderror_rate_fit)
layout(1,1)

# GRID ERROR ~ POINT CLOUD ERROR ----

dust_griderror_point<-data.frame(dust_error[11],dust_error[14])
plot(dust_griderror_point)

#Create regression fits
fit1<-lm(Grid.Error~Point.Cloud.Error,dust_griderror_point)
fit2<-lm(Grid.Error~poly(Point.Cloud.Error,2,raw=TRUE),dust_griderror_point)
fit3<-lm(Grid.Error~poly(Point.Cloud.Error,3,raw=TRUE),dust_griderror_point)
fit4<-lm(Grid.Error~poly(Point.Cloud.Error,4,raw=TRUE),dust_griderror_point)
fit5<-lm(Grid.Error~poly(Point.Cloud.Error,5,raw=TRUE),dust_griderror_point)

#Plot fits
x<-seq(5,8.5,length=40)
lines(x,predict(fit1, data.frame(Point.Cloud.Error=x)), col="red")
lines(x,predict(fit2, data.frame(Point.Cloud.Error=x)), col="blue")
lines(x,predict(fit3, data.frame(Point.Cloud.Error=x)), col="green")
lines(x,predict(fit4, data.frame(Point.Cloud.Error=x)), col="orange")
lines(x,predict(fit5, data.frame(Point.Cloud.Error=x)), col="magenta")

summary(fit1)
summary(fit2)
summary(fit3)
summary(fit4)
summary(fit5)

#Analysis of variance between fits
anova(fit1,fit2)
anova(fit2,fit3)
anova(fit3,fit4)
anova(fit4,fit5)

#Plot fit 1
dust_griderror_point_fit<-fit1
layout(matrix(c(1,2,3,4),2,2))
plot(dust_griderror_point_fit)
layout(1,1)

# GRID ERROR ~ POINT CLOUD ERROR ~ RATE ----

dust_griderror_point_rate<-data.frame(dust_error[2],dust_error[11],dust_error[14])
plot(dust_griderror_point_rate)

#Create fit
fit1<-lm(Grid.Error~Point.Cloud.Error+Dust.Rate,dust_griderror_point_rate)
summary(fit1)
dust_griderror_point_rate_fit<-fit1

#Plot fit
layout(matrix(c(1,2,3,4),2,2))
plot(fit1)
layout(1,1)

# COLLISION ~ GRID ERROR ----

dust_collision_grid<-data.frame(dust_error[14],dust_error[8])
plot(dust_collision_grid$Grid.Error,dust_collision_grid$Collision)

#Create fit
dust_collision_grid$Collision.factor<-factor(dust_collision_grid$Collision, labels=c("N","Y"))
fit<-glm(Collision.factor~Grid.Error, dust_collision_grid, family = binomial)
dust_Collision_grid_fit<-fit

#Plot conditional density
dust_collision_grid$Collision.factor<-factor(dust_collision_grid$Collision, labels=c("N","Y"))
cdplot(Collision.factor~Grid.Error, dust_collision_grid)
summary(fit)

#Hoslem test
hoslem.test(dust_collision_grid$Collision,fitted(fit))

#Plot fit
layout(matrix(c(1,2,3,4),2,2))
plot(fit)
layout(1,1)

# COLLISION ~ SPEED ----

dust_collision_speed<-data.frame(dust_error[10],dust_error[8])
plot(dust_collision_speed)

#Create Fit
dust_collision_speed$Collision.factor <- factor(dust_collision_speed$Collision, labels=c("N", "Y"))
fit<-glm(Collision.factor~Effective.Speed,dust_collision_speed,family="binomial")

#Plot Conditional Density
dust_collision_speed$Collision.factor <- factor(dust_collision_speed$Collision, labels=c("N", "Y"))
cdplot(Collision.factor~Effective.Speed,dust_collision_speed)

summary(fit)

#Hoslem test
hoslem.test(dust_collision_speed$Collision,fitted(fit))

#Plot fit
layout(matrix(c(1,2,3,4),2,2))
plot(fit)
layout(1,1)

# COLLISION ~ SPEED ~ GRID ERROR ----

dust_collision_speed_grid<-data.frame(dust_error[10],dust_error[14],dust_error[8])
plot(dust_collision_speed_grid)

#Create fit
fit<-glm(Collision~Effective.Speed+Grid.Error,dust_collision_speed_grid,family="binomial")

summary(fit)

#Plot Conditional Density
dust_collision_speed_grid$Collision.factor<-factor(dust_collision_speed_grid$Collision, labels=c("N","Y"))
plot(Collision.factor~Effective.Speed+Grid.Error,dust_collision_speed_grid)

#Hoslem test
hoslem.test(dust_collision_speed_grid$Collision,fitted(fit))

#Plot fit
layout(matrix(c(1,2,3,4),2,2))
plot(fit)
layout(1,1)

# RATE ~ GRID ERROR ----

dust_collision_rate<-data.frame(dust_error[2],dust_error[8])
plot(dust_collision_rate)

#Create Fit
fit<-glm(Collision~Dust.Rate,dust_collision_rate,family="binomial")

summary(fit)

#Plot Conditional Density
dust_collision_rate$Collision.factor <- factor(dust_collision_rate$Collision, labels=c("N", "Y"))
cdplot(Collision.factor~Dust.Rate,dust_collision_rate)

#Hoslem test
hoslem.test(dust_collision_rate$Collision,fitted(fit))

#Plot fit
layout(matrix(c(1,2,3,4),2,2))
plot(fit)
layout(1,1)


#LDA Model ----

smp_size<-floor(0.6 * nrow(dust_error))

set.seed(158)

tdust_ind<-sample(seq_len(nrow(dust_error)), size = smp_size)

tdust<-dust_error[tdust_ind, ]
test<-dust_error[-tdust_ind, ]
list(tdust)
list(test)

attach(tdust)
fit<-glm(Collision~Path.Error+Effective.Speed+Point.Cloud.Error,data=tdust,family="binomial")
summary(fit)
detach(tdust)

###Predict fit tdusting data and find tdusting accuracy
pred.prob = predict(fit, type="response")
pred.prob = ifelse(pred.prob > 0.5, 1, 0)
table(pred.prob, tdust$Collision)

##Predict fit test Data and find the test accuracy.
attach(test)
pred.prob = predict(fit, newdata= test, type="response")
pred.prob = ifelse(pred.prob > 0.5, 1, 0)
table(pred.prob, test$Collision)
detach(test)

attach(tdust)
lda.model<-lda(Collision~Grid.Error+Effective.Speed+Point.Cloud.Error+Path.Error,data=tdust)
lda.model

##Predicting tdusting results.
predmodel.tdust.lda = predict(lda.model, data=tdust)
table(Predicted=predmodel.tdust.lda$class, Collide=tdust$Collision)
detach(tdust)

attach(test)
predmodel.test.lda = predict(lda.model, newdata=test)
table(Predicted=predmodel.test.lda$class, Collide=test$Collision)

plot(lda.model)

par(mfrow=c(1,1))
plot(predmodel.test.lda$x[,1], predmodel.test.lda$class, col=test$Collision+10)

tdust$Collision.factor <- factor(tdust$Collision, labels=c("N", "Y"))
partimat(Collision.factor~Grid.Error+Effective.Speed+Point.Cloud.Error+Path.Error, data=tdust, method="lda")
detach(test)
